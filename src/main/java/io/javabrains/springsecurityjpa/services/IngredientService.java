package io.javabrains.springsecurityjpa.services;

import io.javabrains.springsecurityjpa.models.Ingredient;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface IngredientService {
    List<Ingredient> listAll();
    Ingredient getById(Integer id);
    Ingredient saveOrUpdate(Ingredient ingredient);
    void delete(Integer id);
}
