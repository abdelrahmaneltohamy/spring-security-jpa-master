package io.javabrains.springsecurityjpa.services;

import io.javabrains.springsecurityjpa.models.Cart;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface CartService {
    List<Cart> listAll();
    Cart getById(Integer id);
    Cart saveOrUpdate(Cart cart);
    void delete(Integer id);
    Cart getByUserName(String userName);
    List<Cart> getAllByUserName(String userName);
    List<Cart> getAllByUserId(int id);

}
