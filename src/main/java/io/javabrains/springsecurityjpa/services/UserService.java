package io.javabrains.springsecurityjpa.services;

import io.javabrains.springsecurityjpa.models.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {
    List<User> listAll();
    User getById(Integer id);
    Optional<User> getByUserName(String userName);
    User saveOrUpdate(User user);
    void delete(Integer id);
}
