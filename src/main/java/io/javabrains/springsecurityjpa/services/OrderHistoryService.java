package io.javabrains.springsecurityjpa.services;

import io.javabrains.springsecurityjpa.models.OrderHistory;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface OrderHistoryService {
    List<OrderHistory> listAll();
    OrderHistory getById(Integer id);
    OrderHistory saveOrUpdate(OrderHistory orderHistory);
    void delete(Integer id);
}
