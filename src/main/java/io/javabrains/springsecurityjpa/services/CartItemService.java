package io.javabrains.springsecurityjpa.services;

import io.javabrains.springsecurityjpa.models.CartItem;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface CartItemService {
    List<CartItem> listAll();
    CartItem getById(Integer id);
    CartItem saveOrUpdate(CartItem cartItem);
    void delete(Integer id);
}
