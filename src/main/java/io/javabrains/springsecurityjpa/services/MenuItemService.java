package io.javabrains.springsecurityjpa.services;

import io.javabrains.springsecurityjpa.models.MenuItem;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface MenuItemService {
    List<MenuItem> listAll();
    MenuItem getById(Integer id);
    MenuItem saveOrUpdate(MenuItem menuItem);
    void delete(Integer id);
}
