package io.javabrains.springsecurityjpa.enums;

public enum OrderStatus {
    NEW, ALLOCATED, SHIPPED
}
