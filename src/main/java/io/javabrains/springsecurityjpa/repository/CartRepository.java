package io.javabrains.springsecurityjpa.repository;

import io.javabrains.springsecurityjpa.models.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart,Integer> {
    public Cart findByUserUserName(String userName);
    public List<Cart> findAllByUserUserName(String userName);
    public List<Cart> findAllByUserId(int id);
}
