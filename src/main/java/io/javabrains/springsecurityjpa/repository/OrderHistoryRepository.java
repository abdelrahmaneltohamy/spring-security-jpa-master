package io.javabrains.springsecurityjpa.repository;

import io.javabrains.springsecurityjpa.models.OrderHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderHistoryRepository extends JpaRepository<OrderHistory,Integer> {
}
