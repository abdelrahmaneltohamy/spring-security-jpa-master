package io.javabrains.springsecurityjpa.repository;

import io.javabrains.springsecurityjpa.models.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuItemRepository extends JpaRepository<MenuItem,Integer> {
}
