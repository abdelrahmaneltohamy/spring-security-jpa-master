package io.javabrains.springsecurityjpa.repository;

import io.javabrains.springsecurityjpa.models.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartItemRepository extends JpaRepository<CartItem,Integer> {
}
