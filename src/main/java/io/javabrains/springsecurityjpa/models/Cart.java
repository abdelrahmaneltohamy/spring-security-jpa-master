package io.javabrains.springsecurityjpa.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Cart")
@Embeddable
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
   // @ManyToMany(cascade = CascadeType.MERGE/*, mappedBy = "cart"*/)
   // private List<CartItem> cartItems;
   @ManyToMany(cascade = CascadeType.MERGE/*, mappedBy = "cart"*/)
    private  List<MenuItem> menuItems;
    @ManyToOne
    private User user;

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    //@ManyToOne
    //private OrderHistory orderHistory;

    /*public OrderHistory getOrderHistory() {
        return orderHistory;
    }

    public void setOrderHistory(OrderHistory orderHistory) {
        this.orderHistory = orderHistory;
    }*/

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

/*    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }*/
    /*public void addCartItem(CartItem cartItem){
        cartItems.add(cartItem);
        cartItem.setCart(this);
    }

    public void removeCartItem(CartItem cartItem){
        cartItem.setCart(null);
        this.cartItems.remove(cartItem);
    }*/
}
