package io.javabrains.springsecurityjpa.controllers;

import io.javabrains.springsecurityjpa.models.MenuItem;
import io.javabrains.springsecurityjpa.services.MenuItemService;
import io.javabrains.springsecurityjpa.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MenuResource {
    @Autowired
    private UserService userService;
    @Autowired
    private MenuItemService menuItemService;

    @RequestMapping(value = "/menu")
    public List<MenuItem> getAllitems(){
        return menuItemService.listAll();
    }

    @RequestMapping(value = "menu/{id}")
    public MenuItem getMenuItem(@PathVariable int id){
        return menuItemService.getById(id);
    }
    @RequestMapping(value = "/menu/edit",method = RequestMethod.POST)
    public MenuItem createMenuItem(@RequestBody MenuItem menuItem){
        return menuItemService.saveOrUpdate(menuItem);
    }
    @RequestMapping(value = "menu/edit/{id}",method = RequestMethod.PUT)
    public MenuItem updateMenuItem(@RequestBody MenuItem menuItem,@PathVariable int id){
        menuItem.setId(id);
        return menuItemService.saveOrUpdate(menuItem);
    }
    @RequestMapping(value = "menu/edit/{id}",method = RequestMethod.DELETE)
    public void deleteMenuItem(@PathVariable int id){
         menuItemService.delete(id);
    }

}
