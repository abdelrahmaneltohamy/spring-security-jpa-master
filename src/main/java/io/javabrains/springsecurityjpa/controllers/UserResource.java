package io.javabrains.springsecurityjpa.controllers;

import io.javabrains.springsecurityjpa.models.AuthenticationResponse;
import io.javabrains.springsecurityjpa.models.MyUserDetails;
import io.javabrains.springsecurityjpa.models.User;
import io.javabrains.springsecurityjpa.serviceImplementations.MyUserDetailsService;
import io.javabrains.springsecurityjpa.services.UserService;
import io.javabrains.springsecurityjpa.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserResource {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtils jwtTokenUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private MyUserDetailsService userDetailsService;
    @RequestMapping(value = "/registration/signup" ,method = RequestMethod.POST)
    public ResponseEntity<?> createNewUser(@RequestBody User user) throws Exception{
        user.setActive(true);
        user.setRoles("ROLE_USER");
        userService.saveOrUpdate(user);
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }
        //final MyUserDetails MyUserDetails=user.map(MyUserDetails::new).get();
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(user.getUserName());
        //maybe will need it to be myUserDetails
        final MyUserDetails MyUserDetails=(io.javabrains.springsecurityjpa.models.MyUserDetails)userDetails;
        final String jwt = jwtTokenUtil.generateToken(MyUserDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody User user) throws Exception {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }
        //final MyUserDetails MyUserDetails=user.map(MyUserDetails::new).get();
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(user.getUserName());
        //maybe will need it to be myUserDetails
        final MyUserDetails MyUserDetails=(io.javabrains.springsecurityjpa.models.MyUserDetails)userDetails;
        final String jwt = jwtTokenUtil.generateToken(MyUserDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }
   /* @RequestMapping(value = "/user/logout")
    public ResponseEntity<?> logoutUser(@RequestBody User user){
        jwtTokenUtil.deleteToken(userService.getByUserName(user.getUserName()).getJwt());
        return ResponseEntity.ok(user.getUserName()+"Is deleted");
    }*/
}
