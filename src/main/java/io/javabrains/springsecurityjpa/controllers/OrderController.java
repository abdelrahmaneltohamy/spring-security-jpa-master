package io.javabrains.springsecurityjpa.controllers;

import io.javabrains.springsecurityjpa.models.*;
import io.javabrains.springsecurityjpa.models.CartItem;
import io.javabrains.springsecurityjpa.services.*;
import io.javabrains.springsecurityjpa.services.CartItemService;
import net.bytebuddy.implementation.bytecode.Throw;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class OrderController {
    @Autowired
    CartItemService cartItemService;
    @Autowired
    CartService cartService;
    @Autowired
    UserService userService;
    @Autowired
    MenuItemService menuItemService;
    @Autowired
    OrderHistoryService orderHistoryService;

  /*  @RequestMapping(value = "/user/{username}/cart/{id}")
    public Cart getCart(@PathVariable String username,@PathVariable int id){

        return cartService.getById(id);
    }*/
    @RequestMapping(value = "/cart/{username}")
    public Cart getCartByUserName(@PathVariable String username){
        return getCurrentOrder(username);
    }

    @RequestMapping(value = "/cart/{username}/history")
    public List<Cart> getAllOrdersForUser(@PathVariable String username){
        User user=userService.getByUserName(username).orElseThrow(()->{return new EntityNotFoundException("Not found: " + username);});
        return cartService.getAllByUserId(user.getId());
    }

    @RequestMapping(value = "/cart/{username}",method = RequestMethod.POST)
    public Cart createCart(@RequestBody Cart cart,@PathVariable String username){
        User user=userService.getByUserName(username).orElseThrow(()->{return new EntityNotFoundException("Not found: " + username);});
        List<MenuItem> list=new ArrayList<>();
        for(int i=0;i<cart.getMenuItems().size();i++){
            list.add(menuItemService.getById(cart.getMenuItems().get(i).getId()));
        }
        //list.add(menuItemService.getById(cart.getMenuItems().get(0).getId()));
        cart.setMenuItems(list);
        cart.setUser(user);
        Cart temp=cartService.saveOrUpdate(cart);
        return temp;
    }
    @RequestMapping(value="/cart/{username}",method = RequestMethod.DELETE)
    public void deleteOrder(@PathVariable String username){
        cartService.delete(getCurrentOrder(username).getId());
    }
/*
    @RequestMapping(value = "/user/{id}/cart")
    public List<CartItem> getAllitems(@PathVariable int id){
       // cartService.getById(id).getCartItems();
       return null;
        // return cartService.getById(id).getCartItems();
    }

    @RequestMapping(value = "/user/{id}/cart/{cartItemId}")
    public CartItem getCartItem(@PathVariable int id, @PathVariable int cartItemId){
        return null;
        //if(cartService.getById(id).getCartItems().contains(cartItemService.getById(cartItemId)))
        //    return cartItemService.getById(id);
        //else
         //   return null;
    }
    @RequestMapping(value = "/user/{username}/cart/{id}",method = RequestMethod.POST)
    public CartItem createCartItem(@RequestBody CartItem cartItem,@PathVariable int id){
        CartItem temp=cartItemService.saveOrUpdate(cartItem);
     //   cartService.getById(id).addCartItem(temp);
        return temp;
    }
    @RequestMapping(value = "/user/{id}/cart/{cartItemId}",method = RequestMethod.PUT)
    public CartItem updateCartItem(@RequestBody CartItem cartItem,@PathVariable int id,@PathVariable int cartItemId){
        cartItem.setId(cartItemId);
        return cartItemService.saveOrUpdate(cartItem);
    }
    @RequestMapping(value = "/user/{id}/cart/{cartItemId}",method = RequestMethod.DELETE)
    public void deleteCartItem(@PathVariable int id, @PathVariable int cartItemId){
    //    cartService.getById(id).removeCartItem(cartItemService.getById(cartItemId));
        cartItemService.delete(cartItemId);
    }*/
    public Cart getCurrentOrder(String username){
        User user=userService.getByUserName(username).orElseThrow(()->{return new EntityNotFoundException("Not found: " + username);});
        List<Cart> carts=cartService.getAllByUserId(user.getId());
        if(carts.size()>0)
            return carts.get(carts.size()-1);
        return null;
    }
    //tests only method
    public void setServices(CartService cartService,UserService userService,MenuItemService menuItemService){
        this.cartService=cartService;
        this.menuItemService=menuItemService;
        this.userService=userService;
    }

}
