package io.javabrains.springsecurityjpa.serviceImplementations;

import io.javabrains.springsecurityjpa.models.OrderHistory;
import io.javabrains.springsecurityjpa.models.User;
import io.javabrains.springsecurityjpa.repository.OrderHistoryRepository;
import io.javabrains.springsecurityjpa.repository.UserRepository;
import io.javabrains.springsecurityjpa.services.OrderHistoryService;
import io.javabrains.springsecurityjpa.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
@Service
public class OrderHistoryServiceImplementation implements OrderHistoryService   {
    @Autowired
    OrderHistoryRepository orderHistoryRepository;
    @Override
    public List<OrderHistory> listAll() {
        return orderHistoryRepository.findAll();
    }

    @Override
    public OrderHistory getById(Integer id) {
        OrderHistory orderHistory= orderHistoryRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found: " + id));
        return orderHistory;
    }
    

    @Override
    public OrderHistory saveOrUpdate(OrderHistory orderHistory) {
        return orderHistoryRepository.save(orderHistory);
    }

    @Override
    public void delete(Integer id) {
        orderHistoryRepository.deleteById(id);
    }
/*
    @Service
    public static class UserServiceImplementation implements UserService {
        @Autowired
        UserRepository userRepository;
        @Override
        public List<User> listAll() {
            return userRepository.findAll();
        }

        @Override
        public User getById(Integer id) {
            User user= userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("Not found: " + id));
            return user;
        }

        @Override
        public User getByUserName(String userName) {
            Optional<User> user = userRepository.findByUserName(userName);
            return user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));
        }

        @Override
        public User saveOrUpdate(User user) {
            return userRepository.save(user);
        }

        @Override
        public void delete(Integer id) {
            userRepository.deleteById(id);
        }
    }*/
}
