package io.javabrains.springsecurityjpa.serviceImplementations;

import io.javabrains.springsecurityjpa.models.MenuItem;
import io.javabrains.springsecurityjpa.repository.MenuItemRepository;
import io.javabrains.springsecurityjpa.services.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
@Service
public class MenuItemServiceImplementation implements MenuItemService {
    @Autowired
    MenuItemRepository menuItemRepository;
    @Override
    public List<MenuItem> listAll() {
        return menuItemRepository.findAll();
    }

    @Override
    public MenuItem getById(Integer id) {
        MenuItem menuItem= menuItemRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found: " + id));
        return menuItem;
    }


    @Override
    public MenuItem saveOrUpdate(MenuItem menuItem) {
        return menuItemRepository.save(menuItem);
    }

    @Override
    public void delete(Integer id) {
        menuItemRepository.deleteById(id);
    }

    public void setMenuItemRepository(MenuItemRepository menuItemRepository) {
        this.menuItemRepository = menuItemRepository;
    }
}
