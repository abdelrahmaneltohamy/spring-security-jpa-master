package io.javabrains.springsecurityjpa.serviceImplementations;

import io.javabrains.springsecurityjpa.models.Ingredient;
import io.javabrains.springsecurityjpa.repository.IngredientRepository;
import io.javabrains.springsecurityjpa.services.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public class IngredientServiceImplementation implements IngredientService {
    @Autowired
    IngredientRepository ingredientRepository;
    @Override
    public List<Ingredient> listAll() {
        return ingredientRepository.findAll();
    }

    @Override
    public Ingredient getById(Integer id) {
        Ingredient ingredient= ingredientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found: " + id));
        return ingredient;
    }


    @Override
    public Ingredient saveOrUpdate(Ingredient ingredient) {
        return ingredientRepository.save(ingredient);
    }

    @Override
    public void delete(Integer id) {
        ingredientRepository.deleteById(id);
    }
}
