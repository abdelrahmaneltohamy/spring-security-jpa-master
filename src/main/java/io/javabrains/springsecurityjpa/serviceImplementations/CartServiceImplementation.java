package io.javabrains.springsecurityjpa.serviceImplementations;

import io.javabrains.springsecurityjpa.models.Cart;
import io.javabrains.springsecurityjpa.repository.CartRepository;
import io.javabrains.springsecurityjpa.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
@Service
public class CartServiceImplementation implements CartService {
    @Autowired
    CartRepository cartRepository;
    @Override
    public List<Cart> listAll() {
        return cartRepository.findAll();
    }

    @Override
    public Cart getById(Integer id) {
        Cart cart= cartRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found: " + id));
        return cart;
    }


    @Override
    public Cart saveOrUpdate(Cart cart) {
        return cartRepository.save(cart);
    }

    @Override
    public void delete(Integer id) {
        cartRepository.deleteById(id);
    }

    @Override
    public Cart getByUserName(String userName) {
        return cartRepository.findByUserUserName(userName);
    }

    @Override
    public List<Cart> getAllByUserName(String userName) {
        return cartRepository.findAllByUserUserName(userName);
    }

    @Override
    public List<Cart> getAllByUserId(int id) {
        return cartRepository.findAllByUserId(id);
    }
    //for tests only
    public void setCartRepository(CartRepository cartRepository){
        this.cartRepository=cartRepository;
    }
}
