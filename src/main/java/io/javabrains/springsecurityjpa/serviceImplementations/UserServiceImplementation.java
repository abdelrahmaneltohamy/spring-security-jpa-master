package io.javabrains.springsecurityjpa.serviceImplementations;

import io.javabrains.springsecurityjpa.models.MyUserDetails;
import io.javabrains.springsecurityjpa.models.User;
import io.javabrains.springsecurityjpa.repository.UserRepository;
import io.javabrains.springsecurityjpa.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
@Service
public class UserServiceImplementation implements UserService {

    @Autowired
    UserRepository userRepository;
    @Override
    public List<User> listAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(Integer id) {
        User user= userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found: " + id));
        return user;
    }

    @Override
    public Optional<User> getByUserName(String userName) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(userName);

        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));

        return user;
    }

    @Override
    public User saveOrUpdate(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }
    public void setUserRepository(UserRepository userRepository){
        this.userRepository=userRepository;
    }
}
