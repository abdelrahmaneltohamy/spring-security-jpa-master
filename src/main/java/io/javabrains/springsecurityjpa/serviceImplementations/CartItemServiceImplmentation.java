package io.javabrains.springsecurityjpa.serviceImplementations;

import io.javabrains.springsecurityjpa.models.CartItem;
import io.javabrains.springsecurityjpa.repository.CartItemRepository;
import io.javabrains.springsecurityjpa.services.CartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
@Service
public class CartItemServiceImplmentation implements CartItemService {
    @Autowired
    CartItemRepository cartItemRepository;
    @Override
    public List<CartItem> listAll() {
        return cartItemRepository.findAll();
    }

    @Override
    public CartItem getById(Integer id) {
        CartItem cartItem= cartItemRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found: " + id));
        return cartItem;
    }


    @Override
    public CartItem saveOrUpdate(CartItem cartItem) {
        return cartItemRepository.save(cartItem);
    }

    @Override
    public void delete(Integer id) {
        cartItemRepository.deleteById(id);
    }
}
