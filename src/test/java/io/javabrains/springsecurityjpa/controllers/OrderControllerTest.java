package io.javabrains.springsecurityjpa.controllers;


import io.javabrains.springsecurityjpa.models.Cart;
import io.javabrains.springsecurityjpa.models.MenuItem;
import io.javabrains.springsecurityjpa.models.MyUserDetails;
import io.javabrains.springsecurityjpa.models.User;
import io.javabrains.springsecurityjpa.services.CartService;
import io.javabrains.springsecurityjpa.services.MenuItemService;
import io.javabrains.springsecurityjpa.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.mockito.Spy;
import org.springframework.boot.test.mock.mockito.SpyBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class OrderControllerTest {
    OrderController orderController;
    @Mock
    CartService cartService;
    @Mock
    UserService userService;
    @Mock
    MenuItemService menuItemService;
    @Spy
    OrderController orderControllerspy;
    @BeforeEach
    public void setup(){

        orderControllerspy=spy(OrderController.class);
        orderController=new OrderController();
        cartService=mock(CartService.class);
        menuItemService=mock(MenuItemService.class);
        userService=mock(UserService.class);
    }
    @Test
    public void getCartByUserNameShouldReturnExceptionWhenGetCartByUserNameHasEmptyUser(){
        Optional<User> user=Optional.empty();
         when(userService.getByUserName("maun")).thenReturn(user);
        orderController.setServices(cartService,userService,menuItemService);
        assertThrows(EntityNotFoundException.class,()->orderController.getCartByUserName("maun"));
    }
    @Test
    public  void getCartByUserNameShouldGetCartByUserName(){
        Cart cart=new Cart();
        List<Cart> carts=new ArrayList<>();
        carts.add(cart);
        Optional<User> user=Optional.ofNullable(new User(1));
        when(userService.getByUserName("maun")).thenReturn(user);
        when(cartService.getAllByUserId(1)).thenReturn(carts);
        orderController.setServices(cartService,userService,menuItemService);
        Cart reCart=orderController.getCartByUserName("maun");
        assertNotNull(reCart);
    }
    @Test
    public void getCartByUserNameShouldReturnNullIfCartWasEmpty(){
        List<Cart> carts=new ArrayList<>();
        Optional<User> user=Optional.ofNullable(new User(1));
        when(userService.getByUserName("maun")).thenReturn(user);
        when(cartService.getAllByUserId(1)).thenReturn(carts);
        orderController.setServices(cartService,userService,menuItemService);
        Cart reCart=orderController.getCartByUserName("maun");
        assertNull(reCart);
    }
    @Test
    public void getAllOrdersForUserShouldReturnExceptionWhenGetCartByUserNameHasEmptyUser(){
        Optional<User> user=Optional.empty();
        when(userService.getByUserName("maun")).thenReturn(user);
        orderController.setServices(cartService,userService,menuItemService);
        assertThrows(EntityNotFoundException.class,()->orderController.getAllOrdersForUser("maun"));
    }
    @Test
    public void getAllOrdersForUserShouldReturnNullIfNoOrderIsDoneForUser(){
        List<Cart> carts=new ArrayList<>();
        Optional<User> user=Optional.ofNullable(new User(1));
        when(userService.getByUserName("maun")).thenReturn(user);
        when(cartService.getAllByUserId(1)).thenReturn(carts);
        orderController.setServices(cartService,userService,menuItemService);
        List<Cart> recarts=orderController.getAllOrdersForUser("maun");
        assertEquals(carts,recarts);
    }
    @Test
    public void getAllOrdersForUserShouldReturnAListOfCartsIfUserHasOrderHistory(){
        List<Cart> carts=new ArrayList<>();
        Optional<User> user=Optional.ofNullable(new User(1));
        carts.add(new Cart());
        when(userService.getByUserName("maun")).thenReturn(user);
        when(cartService.getAllByUserId(1)).thenReturn(carts);
        orderController.setServices(cartService,userService,menuItemService);
        List<Cart> recarts=orderController.getAllOrdersForUser("maun");
        assertEquals(carts,recarts);
    }
    @Test
    public void createCartShouldCreateAnOrderForADefineUserName(){
        Cart cart=new Cart();
        MenuItem menuItem=new MenuItem();
        menuItem.setId(234);
        List<MenuItem> menuItems=new ArrayList<>();
        menuItems.add(menuItem);
        cart.setMenuItems(menuItems);
        Optional<User> user=Optional.ofNullable(new User(1));
        when(userService.getByUserName("maun")).thenReturn(user);
        when(cartService.saveOrUpdate(cart)).thenReturn(cart);
        orderController.setServices(cartService,userService,menuItemService);
        Cart reCart=orderController.createCart(cart,"maun");
        assertEquals(reCart,cart);
    }

}
