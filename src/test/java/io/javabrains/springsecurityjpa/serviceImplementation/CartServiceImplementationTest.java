package io.javabrains.springsecurityjpa.serviceImplementation;

import io.javabrains.springsecurityjpa.controllers.OrderController;
import io.javabrains.springsecurityjpa.models.Cart;
import io.javabrains.springsecurityjpa.models.User;
import io.javabrains.springsecurityjpa.repository.CartRepository;
import io.javabrains.springsecurityjpa.serviceImplementations.CartServiceImplementation;
import io.javabrains.springsecurityjpa.services.CartService;
import io.javabrains.springsecurityjpa.services.MenuItemService;
import io.javabrains.springsecurityjpa.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

public class CartServiceImplementationTest {
    CartServiceImplementation cartService;
    @Mock
    CartRepository cartRepository;
    @BeforeEach
    public void setup(){
        cartService=new CartServiceImplementation();
        cartRepository=mock(CartRepository.class);
    }
    @Test
    public void listAllShouldReturnAListOfAllAvailbleOrders(){
        List<Cart> carts=new ArrayList<>();
        Cart cart=new Cart();
        cart.setId(12);
        carts.add(cart);
        when(cartRepository.findAll()).thenReturn(carts);
        cartService.setCartRepository(cartRepository);
        assertEquals(cartService.listAll(),carts);
    }
    @Test
    public void getByIdShouldGetASpecificCart(){
        Cart cart=new Cart();
        cart.setId(12);
        Optional<Cart> opCart=Optional.ofNullable(cart);

        when(cartRepository.findById(12)).thenReturn(opCart);
        cartService.setCartRepository(cartRepository);
        assertEquals(cartService.getById(12).getId(),cart.getId());
    }
    @Test
    public void getByIdShouldThrowExceptionIfNotSuchIdIsAvailble(){
        Optional<Cart> opCart=Optional.empty();

        when(cartRepository.findById(12)).thenReturn(opCart);
        cartService.setCartRepository(cartRepository);
        assertThrows(EntityNotFoundException.class,()->cartService.getById(12));
    }
    @Test
    public void saveOrUpdateWillReturnTheSavedCart(){
        Cart cart=new Cart();
        cart.setId(12);

        when(cartRepository.save(cart)).thenReturn(cart);
        cartService.setCartRepository(cartRepository);
        assertEquals(cartService.saveOrUpdate(cart).getId(),cart.getId());
    }
}
