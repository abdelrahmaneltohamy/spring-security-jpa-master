package io.javabrains.springsecurityjpa.serviceImplementation;

import io.javabrains.springsecurityjpa.models.Cart;
import io.javabrains.springsecurityjpa.models.User;
import io.javabrains.springsecurityjpa.repository.CartRepository;
import io.javabrains.springsecurityjpa.repository.UserRepository;
import io.javabrains.springsecurityjpa.serviceImplementations.CartServiceImplementation;
import io.javabrains.springsecurityjpa.serviceImplementations.UserServiceImplementation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceImplementationTest {
    UserServiceImplementation userService;
    @Mock
    UserRepository userRepository;
    @BeforeEach
    public void setup(){
        userService=new UserServiceImplementation();
        userRepository=mock(UserRepository.class);
    }
    @Test
    public void listAllShouldReturnAListOfAllUsers(){
        List<User> users=new ArrayList<>();
        User user=new User();
        user.setId(1);
        users.add(user);
        when(userRepository.findAll()).thenReturn(users);
        userService.setUserRepository(userRepository);
        assertEquals(userService.listAll(),users);
    }
    @Test
    public void getByIdShouldGetASpecificUser(){
        User user=new User();
        user.setId(12);
        Optional<User> opUser=Optional.ofNullable(user);

        when(userRepository.findById(12)).thenReturn(opUser);
        userService.setUserRepository(userRepository);
        assertEquals(userService.getById(12).getId(),user.getId());
    }
    @Test
    public void getByIdShouldThrowExceptionIfNotSuchIdIsAvailble(){
        Optional<User> opUser=Optional.empty();

        when(userRepository.findById(12)).thenReturn(opUser);
        userService.setUserRepository(userRepository);
        assertThrows(EntityNotFoundException.class,()->userService.getById(12));
    }
    @Test
    public void saveOrUpdateWillReturnTheSavedUser(){
        User user=new User();
        user.setId(12);

        when(userRepository.save(user)).thenReturn(user);
        userService.setUserRepository(userRepository);
        assertEquals(userService.saveOrUpdate(user).getId(),user.getId());
    }
}
