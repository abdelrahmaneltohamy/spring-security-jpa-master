package io.javabrains.springsecurityjpa.serviceImplementation;

import io.javabrains.springsecurityjpa.models.MenuItem;
import io.javabrains.springsecurityjpa.repository.MenuItemRepository;
import io.javabrains.springsecurityjpa.serviceImplementations.MenuItemServiceImplementation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MenuItemServiceImplementationTest {
        MenuItemServiceImplementation menuItemService;
        @Mock
        MenuItemRepository menuItemRepository;
        @BeforeEach
        public void setup(){
            menuItemService=new MenuItemServiceImplementation();
            menuItemRepository=mock(MenuItemRepository.class);
        }
        @Test
        public void listAllShouldReturnAListOfAllMenuItems(){
            List<MenuItem> menuItems=new ArrayList<>();
            MenuItem menuItem=new MenuItem();
            menuItem.setId(1);
            menuItems.add(menuItem);
            when(menuItemRepository.findAll()).thenReturn(menuItems);
            menuItemService.setMenuItemRepository(menuItemRepository);
            assertEquals(menuItemService.listAll(),menuItems);
        }
        @Test
        public void getByIdShouldGetASpecificMenuItem(){
            MenuItem menuItem=new MenuItem();
            menuItem.setId(12);
            Optional<MenuItem> opMenuItem=Optional.ofNullable(menuItem);

            when(menuItemRepository.findById(12)).thenReturn(opMenuItem);
            menuItemService.setMenuItemRepository(menuItemRepository);
            assertEquals(menuItemService.getById(12).getId(),menuItem.getId());
        }
        @Test
        public void getByIdShouldThrowExceptionIfNotSuchIdIsAvailble(){
            Optional<MenuItem> opMenuItem=Optional.empty();

            when(menuItemRepository.findById(12)).thenReturn(opMenuItem);
            menuItemService.setMenuItemRepository(menuItemRepository);
            assertThrows(EntityNotFoundException.class,()->menuItemService.getById(12));
        }
        @Test
        public void saveOrUpdateWillReturnTheSavedMenuItem(){
            MenuItem menuItem=new MenuItem();
            menuItem.setId(12);

            when(menuItemRepository.save(menuItem)).thenReturn(menuItem);
            menuItemService.setMenuItemRepository(menuItemRepository);
            assertEquals(menuItemService.saveOrUpdate(menuItem).getId(),menuItem.getId());
        }
    }


